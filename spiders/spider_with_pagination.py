# -*- coding: utf-8 -*-
import scrapy
class HallroadSpider(scrapy.Spider):
    name = 'hallroad2'
    def __init__(self, name=None, **kwargs):
       self.start_urls = []
       self.start_urls.extend(['https://hallroad.org/arduino-components-page-%s.html'% page for page in   range(1,80)])
       self.start_urls.extend(['https://hallroad.org/covid-19-products-page-%s.html'% page for page in   range(1,3)])
       self.start_urls.extend(['https://hallroad.org/tools-page-%s.html'% page for page in   range(1,31)])
       self.start_urls.extend(['https://hallroad.org/consumer-electronics-page-%s.html'% page for page in   range(1,11)])
       self.start_urls.extend(['https://hallroad.org/computer-and-office-page-%s.html'% page for page in   range(1,5)])
       self.start_urls.extend(['https://hallroad.org/commercial-and-industrial-page-%s.html'% page for page in   range(1,20)])
       self.start_urls.extend(['https://hallroad.org/car-electronics-page-%s.html'% page for page in   range(1,2)])
       self.start_urls.extend(['https://hallroad.org/toys-and-hobbies-page-%s.html'% page for page in   range(1,2)])
      
       


    def parse(self, response):
        self.log('I just visited:' + response.url)
        
        for title in response.css('div.grid-list >div '):
         t=title.css('a.product-title::text').extract()
         p=title.css('span.ty-price-num::text').extract()
         u=title.css("a.product-title::attr('href')").extract()
         yield {
        'Product_Title': t ,
        'Product_Price': p ,
        'Request_url':u,                          }
       

