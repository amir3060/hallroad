# -*- coding: utf-8 -*-
import scrapy


class HallroadSpider(scrapy.Spider):
    name = 'hallroad'
    start_urls = ['https://hallroad.org/arduino-boards-en.html',
                  'https://hallroad.org/covid-19-products.html',
                  'https://hallroad.org/consumer-electronics.html',
                  'https://hallroad.org/computer-and-office.html',
                  'https://hallroad.org/commercial-and-industrial.html',
                  'https://hallroad.org/car-electronics.html',
                  'https://hallroad.org/toys-and-hobbies.html',]

    def parse(self, response):
        
    
        for title in response.css('div.grid-list >div '):
         t=title.css('a.product-title::text').extract()
         p=title.css('span.ty-price-num::text').extract()
         u=title.css("a.product-title::attr('href')").extract()

       
         yield {
        'Product_Title': t ,
        'Product_Price': p ,
        'Request_url':u,
                                   }

        
                    
